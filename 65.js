// //splice method
// //start ,delete, insert
// const myArray=['item1','item2','item3'];
// // //delete
// const deleteditem=myArray.splice(1,2);
// console.log("deleted item",deleteditem);
// // //insert
// // myArray.splice(1,0,'inserted item');
const myArray=['item1','item2','item3'];
const deleteditem=myArray.splice(1,2,"inserted item","inserted item2");
console.log("deleted item",deleteditem);
console.log(myArray);
