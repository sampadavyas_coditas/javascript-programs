//method
//function inside object
function personInfo()
    {
        console.log("person name is harshit and person age is 8");
        console.log(`person name is ${this.firstName} and person age is ${this.age}`);
    }
const person1={
    firstName:"harsh",
    age:8,
    about:personInfo
}
const person2={
    firstName:"harsh",
    age:8,
    about:personInfo
}
const person3={
    firstName:"nitish",
    age:17,
    about:personInfo
}
person1.about();
person2.about();
person3.about();

