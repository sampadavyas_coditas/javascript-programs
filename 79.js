const userMethods={
    about:function()
    {
        return `${this.firstName} is ${this.age} years old`;

    },
    is18:function()
    {
        return this.age>=18;
    },
    sing:function()
    {
        return 'toon nananana';
    }
}

function createUser(firstName,lastName,email,age,address)
{
    const user={};
    user.firstName=firstName;
    user.lastName=lastName;
    user.email=email;
    user.age=age;
    user.address=address;
    user.about=userMethods.about;
    user.is18=userMethods.is18;
    user.sing=userMethods.sing;
    return user;
}
const user1=createUser('harshit','vaishishth','harshi@gmail.com',5,"my address");
const user2=createUser('harshit','vaishishth','harshi@gmail.com',50,"my address");
const user3=createUser('harshit','vaishishth','harshi@gmail.com',5,"my address");
console.log(user1.about());
console.log(user3.about());
console.log(user3.sing());