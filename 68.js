// // Maps
// // map is an iterable

// // store data in ordered fashion

// // store key value pair (like object)
// // duplicate keys are not allowed like objects


// // different between maps and objects

// // objects can only have string or symbol
// // as key 

// // in maps you can use anything as key
// // like array, number, string 

// // object literal 
// // key -> string 
// // key -> symbol
// const person={
//     firstName:"harhit",
//     age:7,
//     1:"one"
// }
// //console.log(person.firstName);
// //console.log(person["firstName"]);
// console.log(person[1]);
// for(let key in person)
// {
//     console.log(typeof key);
// }
// //key value pair
// const person=new Map();
// person.set('firstNmae','harshit');
// person.set('age',7);
// person.set([1,2,3],'onetwoThree');
// person.set({1:'one'},'onetwoThree');
// person.set(1,'one');//1 is number
// console.log(person);
// console.log(person.get(1));
// for(let key of person.keys())
// {
//     console.log(key,typeof key);
// }
//obj we cannot use for of loop
//but  map is iterable
// for(let [key,value] of person)
// {
//     console.log(Array.isArray(key));
//     console.log(key,value)
// }

const person1={
id:1,
firstName:"harshit",
}
const person2={
    id:2,
    firstName:"harsh",
    }
const extraInfo=new Map();
extraInfo.set(person1,{age:8,gender:"male"});
extraInfo.set(person2,{age:9,gender:"female"});
// const person=new Map[['firstName','harshit'],['age',7]]
// console.log(userInfo);
console.log(person1.id);
console.log(extraInfo.get(person1).gender);
console.log(extraInfo.get(person2).gender);
